<?php

require("../includes/admin.inc.php");

if(!isset($_SESSION['user_id']))
{
	header("Location: ../index.php");
	exit();
}

if($_SESSION['userlevel'] == 0)
{
	$result = getLatestUsers($DB);
	$subjects = getLatestSubjects($DB);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="../css/style.css">
	<title>Administrator Home</title>
</head>
<body>
	<div class="wrapper">
		<!-- HEADER -->
		<div class="header">
			<div class="container">
				<div class="logo">
					<h1><a href="../index.php">Attendance Manager</a></h1>
					<p>Admin Control Panel</p>
				</div>
				<div class="links">
					<ul class="top-links">
						<li><span>Welcome, <?php echo $_SESSION['username'];?></span></li>
						<li><a href="../logout.php" class="button button-primary">Log Out</a></li>
					</ul>
				</div>
			</div>
		</div>

		<!-- CONTENT AREA -->
		<div class="content-wrapper">
			<div class="container">
				<div class="content">
					<div class="sidebar">
						<ul class="side-links">
							<li><a href="manageUsers.php" class="button button-full-width button-side-links">Manage Users</a></li>
							<li><a href="manageSubjects.php" class="button button-full-width button-side-links">Manage Subjects</a></li>
						</ul>
					</div>
					<div class="main-content">
						<div class="assigned-classes">
							<h2>Latest Users</h2>
							<div class="table-large">
								<table> 
									<thead>
										<tr>
											<th>User ID</th>
											<th>User Name</th>
											<th>User Level</th>
											<th>Options</th>
										</tr>
									</thead>
									<tbody>
										<?php
											$count=1;
											foreach($result as $s)
											{
												if($count%2 == 0)
												{
													echo '<tr class="odd">';
												}
												else
												{
													echo '<tr class="even">';
												}

												echo '<td>' . $s['user_id'] . '</td>';
												echo '<td>' . $s['username'] . '</td>';
												$s['userlevel'] = ($s['userlevel'] == 2) ? 'Student' : 'Faculty';
												echo '<td>' . $s['userlevel'] . '</td>';
												echo '<td><a href="editUser.php?uid=' . $s['user_id'] . '">Edit</a></td>';
												echo '</tr>';
												$count++;
											}
										?>
									</tbody>
								</table>
							</div>
						</div>
						<div class="assigned-classes">
							<h2>Latest Subjects</h2>
							<div class="table-large">
								<table> 
									<thead>
										<tr>
											<th>Subject ID</th>
											<th>Subject Name</th>
											<th>Department</th>
											<th>Semester</th>
											<th>Options</th>
										</tr>
									</thead>
									<tbody>
										<?php
											$count=1;
											foreach($subjects as $s)
											{
												if($count%2 == 0)
												{
													echo '<tr class="odd">';
												}
												else
												{
													echo '<tr class="even">';
												}

												echo '<td>' . $s['subject_code'] . '</td>';
												echo '<td>' . $s['subject_name'] . '</td>';
												echo '<td>' . getDepartment($s['dept_code']) . '</td>';
												echo '<td>' . $s['semester'] . '</td>';
												echo '<td><a href="editSubject.php?subject=' . $s['subject_code'] . '">Edit</a></td>';
												echo '</tr>';
												$count++;
											}
										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		
		<!-- FOOTER -->
		<div class="footer">
			<span class="copyright">Copyright </span>
			<span class="year"> 2014</span>
		</div>
	</div>
</body>
</html>
<?php

}

else
{
	header('HTTP/1.0 403 Forbidden');
	echo '403 Forbidden';
	exit();
}