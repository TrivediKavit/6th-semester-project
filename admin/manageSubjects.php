<?php

require("../includes/admin.inc.php");

if(!isset($_SESSION['user_id']))
{
	header("Location: ../index.php");
	exit();
}

if($_SESSION['userlevel'] == 0)
{
	//FORM HANDLING
	if(isset($_POST) && !empty($_POST))
	{
		if(isset($_POST['createSubjectCommit']))
		{
			$errors = array();
			if(!is_numeric($_POST['subject_code']))
			{
				$errors[] = "Subject Code should be numeric.";
			}
			
			if(!is_numeric($_POST['subject_credits']))
			{
				$errors[] = "Subject Credits should be numeric.";
			}

			if(empty($_POST['subject_code']) || ($_POST['subject_name'] == "") || empty($_POST['subject_credits']))
			{
				$errors[] = "All Fields are mandatory.";
			}

			if(empty($errors))
			{
				createSubject($_POST,$DB);
				header("Location: manageSubjects.php#!");
			}
			else
			{
				echo "<ul>";
				foreach($errors as $e)
				{
					echo "<li>{$e}</li>";
				}
				echo "</ul>";
				echo '<a href="manageSubjects.php">Click Here to Go Back</a>';
			}
			
		}
		else
		{
			header('HTTP/1.0 403 Forbidden');
			echo '403 Forbidden';
		}
		exit();
	}
	$subjects = getAllSubjects($DB);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="../css/style.css">
	<title>Manage Subjects</title>
</head>
<body>
	<div class="wrapper">
		<!-- HEADER -->
		<div class="header">
			<div class="container">
				<div class="logo">
					<h1><a href="../index.php">Attendance Manager</a></h1>
					<p>Admin Control Panel</p>
				</div>
				<div class="links">
					<ul class="top-links">
						<li><span>Welcome, <?php echo $_SESSION['username'];?></span></li>
						<li><a href="../logout.php" class="button button-primary">Log Out</a></li>
					</ul>
				</div>
			</div>
		</div>

		<!-- CONTENT AREA -->
		<div class="content-wrapper">
			<div class="container">
				<div class="content">
					<div class="sidebar">
						<ul class="side-links">
							<li><a href="manageUsers.php" class="button button-full-width button-side-links">Manage Users</a></li>
							<li><a href="manageSubjects.php" class="button button-full-width button-side-links">Manage Subjects</a></li>
						</ul>
					</div>
					<div class="main-content">
						<div class="assigned-classes">
							<h2>Subjects</h2>
							<div class="table-large">
								<table> 
									<thead>
										<tr>
											<th>Subject Code</th>
											<th>Subject Name</th>
											<th>Department</th>
											<th>Semester</th>
											<th>Options</th>
										</tr>
									</thead>
									<tbody>
										<?php
											$count=1;
											foreach($subjects as $s)
											{
												if($count%2 == 0)
												{
													echo '<tr class="odd">';
												}
												else
												{
													echo '<tr class="even">';
												}

												echo '<td>' . $s['subject_code'] . '</td>';
												echo '<td>' . $s['subject_name'] . '</td>';
												echo '<td>' . getDepartment($s['dept_code']) . '</td>';
												echo '<td>' . $s['semester'] . '</td>';
												echo '<td><a href="editSubject.php?subject=' . $s['subject_code'] . '">Edit</a></td>';
												echo '</tr>';
												$count++;
											}
										?>
									</tbody>
								</table>
							</div>
						</div>
						<p>+ <a href="#modal-create-subject" class="call-modal">Create New Subject</a></p>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		
		<!-- FOOTER -->
		<div class="footer">
			<span class="copyright">Copyright </span>
			<span class="year"> 2014</span>
		</div>

		<!-- CREATE NEW USER MODAL -->
		<section class="semantic-content" id="modal-create-subject" tabindex="-1" role="dialog" aria-labelledby="modal-label" aria-hidden="true">

		    <div class="modal-inner">
		        <header id="modal-label"><h2>Create New Subject</h2></header>
		        <div class="modal-content">
		        	<form action="" method="POST">
						<div class="field">
							<label for="subject_code">Subject Code : </label>
							<input type="text" name="subject_code" >
						</div>
						<div class="clear"></div>
						<div class="field">
							<label for="subject_name">Subject Name : </label>
							<input type="text" name="subject_name" >
						</div>
						<div class="clear"></div>
						<div class="field">
							<label for="dept_code">Department : </label>
							<select name="dept_code">
								<option disabled>Select a Department</option>
								<option selected value="7">Computer Engineering</option>
							</select>
						</div>
						<div class="clear"></div>
						<div class="field">
							<label for="semester">Semester : </label>
							<select name="semester">
								<?php
									$options = getSemesters();

									foreach($options as $option)
									{
										echo '<option value="'.$option['value'].'">'.$option['option'].'</option>';
									}
								?>
							</select>
						</div>
						<div class="clear"></div>
						<div class="field">
							<label for="subject_credits">Credits : </label>
							<input type="text" name="subject_credits" />
						</div>
						<div class="clear"></div>
						<div class="field">
							<input class="button button-success" name="createSubjectCommit" type="submit" value="Next">
						</div>
					</form>
		        </div>
		        <footer>
		        	<small>* Please Note that this action not reversible. Continue at your own risk.</small>
		        </footer>
		    </div>

    		<a href="#!" class="modal-close" title="Close this modal" data-close="Close" data-dismiss="modal">×</a>
		</section>

	</div>
</body>
</html>
<?php

}

else
{
	header('HTTP/1.0 403 Forbidden');
	echo '403 Forbidden';
	exit();
}