<?php

require("../includes/admin.inc.php");

if(!isset($_SESSION['user_id']))
{
	header("Location: ../index.php");
	exit();
}

if($_SESSION['userlevel'] == 0)
{
	//FORM HANDLING
	if(isset($_POST) && !empty($_POST))
	{
		if(isset($_POST['createUserCommit']))
		{
			$uid = createUser($_POST,$DB);
			if(is_numeric($uid))
			{
				$_SESSION['uuid']	= $uid;
				$_SESSION['utype']	= $_POST['userlevel'];

				if($_SESSION['utype'] == 1)
				{
					header("Location: manageUsers.php#modal-create-faculty");
				}
				else
				{
					header("Location: manageUsers.php#modal-create-student");
				}
			}
			else
			{
				echo 'Some Error Occured.';
			}
		}
		else if(isset($_POST['createFacultyCommit']))
		{
			$uid = $_SESSION['uuid'];
			createFaculty($uid,$_POST,$DB);
			header("Location: manageUsers.php#!");
		}
		else if(isset($_POST['createStudentCommit']))
		{
			$uid = $_SESSION['uuid'];
			createStudent($uid,$_POST,$DB);
			header("Location: manageUsers.php#!");
		}
		else
		{
			header('HTTP/1.0 403 Forbidden');
			echo '403 Forbidden';
		}
		exit();
	}
	$result = getAllUsers($DB);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="../css/style.css">
	<title>Manage Users</title>
</head>
<body>
	<div class="wrapper">
		<!-- HEADER -->
		<div class="header">
			<div class="container">
				<div class="logo">
					<h1><a href="../index.php">Attendance Manager</a></h1>
					<p>Admin Control Panel</p>
				</div>
				<div class="links">
					<ul class="top-links">
						<li><span>Welcome, <?php echo $_SESSION['username'];?></span></li>
						<li><a href="../logout.php" class="button button-primary">Log Out</a></li>
					</ul>
				</div>
			</div>
		</div>

		<!-- CONTENT AREA -->
		<div class="content-wrapper">
			<div class="container">
				<div class="content">
					<div class="sidebar">
						<ul class="side-links">
							<li><a href="manageUsers.php" class="button button-full-width button-side-links">Manage Users</a></li>
							<li><a href="manageSubjects.php" class="button button-full-width button-side-links">Manage Subjects</a></li>
						</ul>
					</div>
					<div class="main-content">
						<div class="assigned-classes">
							<h2>Users</h2>
							<div class="table-large">
								<table> 
									<thead>
										<tr>
											<th>User ID</th>
											<th>User Name</th>
											<th>User Level</th>
											<th>Options</th>
										</tr>
									</thead>
									<tbody>
										<?php
											$count=1;
											foreach($result as $s)
											{
												if($count%2 == 0)
												{
													echo '<tr class="odd">';
												}
												else
												{
													echo '<tr class="even">';
												}

												echo '<td>' . $s['user_id'] . '</td>';
												echo '<td>' . $s['username'] . '</td>';
												$s['userlevel'] = ($s['userlevel'] == 0) ? 'Admin' : (($s['userlevel'] == 2) ? 'Student' : 'Faculty');
												echo '<td>' . $s['userlevel'] . '</td>';
												echo '<td><a href="editUser.php?uid=' . $s['user_id'] . '">Edit</a></td>';
												echo '</tr>';
												$count++;
											}
										?>
									</tbody>
								</table>
							</div>
						</div>
						<p>+ <a href="#modal-create-user" class="call-modal">Create New User</a></p>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		
		<!-- FOOTER -->
		<div class="footer">
			<span class="copyright">Copyright </span>
			<span class="year"> 2014</span>
		</div>

		<!-- CREATE NEW USER MODAL -->
		<section class="semantic-content" id="modal-create-user" tabindex="-1" role="dialog" aria-labelledby="modal-label" aria-hidden="true">

		    <div class="modal-inner">
		        <header id="modal-label"><h2>Create New User</h2></header>
		        <div class="modal-content">
		        	<form action="" method="POST">
						<div class="field">
							<label for="username">Username : </label>
							<input type="text" name="username" >
						</div>
						<div class="clear"></div>
						<div class="field">
							<label for="password">Password : </label>
							<input type="text" name="password" >
						</div>
						<div class="clear"></div>
						<div class="field">
							<label for="email">Email : </label>
							<input type="text" name="email" >
						</div>
						<div class="clear"></div>
						<div class="field">
							<label for="userlevel">UserLevel : </label>
							<select name="userlevel">
								<option disabled selected>Select a User Level</option>
								<option value="1">Faculty</option>
								<option value="2">Student</option>
							</select>
						</div>
						<div class="clear"></div>
						<div class="field">
							<input class="button button-success" name="createUserCommit" type="submit" value="Next">
						</div>
					</form>
		        </div>
		        <footer>
		        	<small>* Please Note that this action not reversible. Continue at your own risk.</small>
		        </footer>
		    </div>

    		<a href="#!" class="modal-close" title="Close this modal" data-close="Close" data-dismiss="modal">×</a>
		</section>

		<!-- CREATE NEW STUDENT MODAL -->
		<?php if(isset($_SESSION['uuid']) && isset($_SESSION['utype']) && ($_SESSION['utype'] == 2)) { ?>
		<section class="semantic-content" id="modal-create-student" tabindex="-1" role="dialog" aria-labelledby="modal-label" aria-hidden="true">

		    <div class="modal-inner">
		        <header id="modal-label"><h2>Create New Student</h2></header>
		        <div class="modal-content">
		        	<form action="" method="POST">
		        		<div class="field">
							<label for="enrollment_number">Enrollment Number : </label>
							<input type="text" name="enrollment_number" >
						</div>
						<div class="clear"></div>
						<div class="field">
							<label for="firstname">Firstname : </label>
							<input type="text" name="firstname" >
						</div>
						<div class="clear"></div>
						<div class="field">
							<label for="lastname">Lastname : </label>
							<input type="text" name="lastname" >
						</div>
						<div class="clear"></div>
						<div class="field">
							<label for="dept_code">Department : </label>
							<select name="dept_code">
								<option disabled>Select a Department</option>
								<option selected value="7">Computer Engineering</option>
							</select>
						</div>
						<div class="clear"></div>
						<div class="field">
							<label for="semester">Semester : </label>
							<select name="semester">
								<?php
									$options = getSemesters();

									foreach($options as $option)
									{
										echo '<option value="'.$option['value'].'">'.$option['option'].'</option>';
									}
								?>
							</select>
						</div>
						<div class="clear"></div>
						<div class="field">
							<input class="button button-success" name="createStudentCommit" type="submit" value="Next">
						</div>
					</form>
		        </div>
		        <footer>
		        	<small>* Please Note that this action not reversible. Continue at your own risk.</small>
		        </footer>
		    </div>

    		<a href="#!" class="modal-close" title="Close this modal" data-close="Close" data-dismiss="modal">×</a>
		</section>
		<?php } ?>

		<!-- CREATE NEW FACULTY MODAL -->
		<?php if(isset($_SESSION['uuid']) && isset($_SESSION['utype']) && ($_SESSION['utype'] == 1)) { ?>
		<section class="semantic-content" id="modal-create-faculty" tabindex="-1" role="dialog" aria-labelledby="modal-label" aria-hidden="true">

		    <div class="modal-inner">
		        <header id="modal-label"><h2>Create New Faculty</h2></header>
		        <div class="modal-content">
		        	<form action="" method="POST">
						<div class="field">
							<label for="firstname">Firstname : </label>
							<input type="text" name="firstname" >
						</div>
						<div class="clear"></div>
						<div class="field">
							<label for="lastname">Lastname : </label>
							<input type="text" name="lastname" >
						</div>
						<div class="clear"></div>
						<div class="field">
							<label for="dept_code">Department : </label>
							<select name="dept_code">
								<option disabled>Select a Department</option>
								<option selected value="7">Computer Engineering</option>
							</select>
						</div>
						<div class="clear"></div>
						<div class="field">
							<input class="button button-success" name="createFacultyCommit" type="submit" value="Next">
						</div>
					</form>
		        </div>
		        <footer>
		        	<small>* Please Note that this action not reversible. Continue at your own risk.</small>
		        </footer>
		    </div>

    		<a href="#!" class="modal-close" title="Close this modal" data-close="Close" data-dismiss="modal">×</a>
		</section>
		<?php } ?>

	</div>
</body>
</html>
<?php

}

else
{
	header('HTTP/1.0 403 Forbidden');
	echo '403 Forbidden';
	exit();
}