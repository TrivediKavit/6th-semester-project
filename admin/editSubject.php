<?php

require("../includes/admin.inc.php");

if(!isset($_SESSION['user_id']))
{
	header("Location: ../index.php");
	exit();
}

if($_SESSION['userlevel'] == 0)
{
	//FORM HANDLING
	if(isset($_POST) && !empty($_POST))
	{
		if(isset($_POST['subjectCommit']))
		{
			if(isset($_SESSION['subject_code']) && ($_SESSION['subject_code'] === $_POST['subject_code']))
			{
				$errors = array();
				if(!is_numeric($_POST['subject_code']))
				{
					$errors[] = "Subject Code should be numeric.";
				}
			
				if(!is_numeric($_POST['subject_credits']))
				{
					$errors[] = "Subject Credits should be numeric.";
				}

				if(empty($_POST['subject_code']) || ($_POST['subject_name'] == "") || empty($_POST['subject_credits']))
				{
					$errors[] = "All Fields are mandatory.";
				}

				if(empty($errors))
				{
					$subject_code = $_SESSION['subject_code'];
					updateSubject($subject_code,$_POST,$DB);
					header("Location: editSubject.php?subject={$_GET['subject']}#!");
				}
				else
				{
					echo "<ul>";
					foreach($errors as $e)
					{
						echo "<li>{$e}</li>";
					}
					echo "</ul>";
					echo '<a href="editSubject.php?subject=' . $_GET['subject'] . '">Click Here to Go Back</a>';
				}
			}
			else
			{
				echo 'Some Error Occured.';
			}
		}
		else if(isset($_POST['deleteSubjectCommit']))
		{
			if(isset($_SESSION['subject_code']) && ($_SESSION['subject_code'] === $_POST['subject_code']) && ($_POST['subject_code'] === $_GET['subject']))
			{
				$subject_code = $_SESSION['subject_code'];
				deleteSubject($subject_code,$DB);
				header("Location: manageSubjects.php#!");
			}
			else
			{
				echo 'Some Error Occured.';
			}
		}
		else
		{
			header('HTTP/1.0 403 Forbidden');
			echo '403 Forbidden';
		}
		exit();
	}


	if(isset($_GET['subject']) && is_numeric($_GET['subject']))
	{
		$subject_code = $_GET['subject'];
		$_SESSION['subject_code'] = $subject_code;
	}	
	else
	{
		header("Location: manageSubjects.php#!");
		exit();
	}
	$result = getSubjectByCode($subject_code,$DB);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/modal.css">
	<title>Edit Subject Information</title>
</head>
<body>
	<div class="wrapper">
		<!-- HEADER -->
		<div class="header">
			<div class="container">
				<div class="logo">
					<h1><a href="../index.php">Attendance Manager</a></h1>
					<p>Admin Control Panel</p>
				</div>
				<div class="links">
					<ul class="top-links">
						<li><span>Welcome, <?php echo $_SESSION['username'];?></span></li>
						<li><a href="../logout.php" class="button button-primary">Log Out</a></li>
					</ul>
				</div>
			</div>
		</div>

		<!-- CONTENT AREA -->
		<div class="content-wrapper">
			<div class="container">
				<div class="content">
					<div class="sidebar">
						<ul class="side-links">
							<li><a href="manageUsers.php" class="button button-full-width button-side-links">Manage Users</a></li>
							<li><a href="manageSubjects.php" class="button button-full-width button-side-links">Manage Subjects</a></li>
						</ul>
					</div>
					<div class="main-content">
						<div class="personal-details">
							<h2>Subject Information</h2>
							<div class="details">
								<ul>
									<li><span class="bold">Subject Code: </span><?php echo $result['subject_code']; ?></li>
									<li><span class="bold">Subject Name: </span><?php echo $result['subject_name']; ?></li>
									<li><span class="bold">Department: </span><?php echo getDepartment($result['dept_code']); ?></li>
									<li><span class="bold">Semester: </span><?php echo $result['semester']; ?></li>
									<li><span class="bold">Credits: </span><?php echo $result['subject_credits'] ?></li>
								</ul>
							</div>
							<div class="image">
								<img src="http://placehold.it/140X100" alt="Placeholder">
							</div>
						</div>
						<p>
							<a href="#modal-edit-subject" class="call-modal">Edit Subject Information</a> | 
							<a href="#modal-delete-subject" class="call-modal">Delete Subject</a>
						</p>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		
		<!-- FOOTER -->
		<div class="footer">
			<span class="copyright">Copyright </span>
			<span class="year"> 2014</span>
		</div>

		<!-- EDIT SUBJECT MODAL -->
		<section class="semantic-content" id="modal-edit-subject" tabindex="-1" role="dialog" aria-labelledby="modal-label" aria-hidden="true">

		    <div class="modal-inner">
		        <header id="modal-label"><h2>Edit Subject Information</h2></header>
		        <div class="modal-content">
		        	<form action="" method="POST">
						<div class="field">
							<label for="subject_name">Subject Name : </label>
							<input type="text" name="subject_name" value="<?php echo $result['subject_name']; ?>">
						</div>
						<div class="clear"></div>
						<div class="field">
							<label for="subject_credits">Subject Credits : </label>
							<input type="text" name="subject_credits" value="<?php echo $result['subject_credits']; ?>">
						</div>
						<div class="clear"></div>
						<div class="field">
							<label for="dept_code">Department : </label>
							<select name="dept_code">
								<option disabled>Select a Department</option>
								<option selected value="7">Computer Engineering</option>
							</select>
						</div>
						<div class="clear"></div>
						<div class="field">
							<label for="semester">Semester : </label>
							<select name="semester">
								<?php

									$options = getSemesters();

									foreach($options as $option)
									{
										if($option['value'] == $result['semester'])
										{
											echo '<option selected value="'.$option['value'].'">'.$option['option'].'</option>';
										}
										else
										{
											echo '<option value="'.$option['value'].'">'.$option['option'].'</option>';
										}
									}
								?>
							</select>
						</div>
						<div class="clear"></div>
						<input type="hidden" name="subject_code" value="<?php echo $subject_code; ?>">
						<div class="field">
							<input class="button button-success" name="subjectCommit" type="submit" value="Commit">
						</div>
					</form>
		        </div>
		        <footer>
		        	<small>* Please Note that this action not reversible. Continue at your own risk.</small>
		        </footer>
		    </div>

    		<a href="#!" class="modal-close" title="Close this modal" data-close="Close" data-dismiss="modal">×</a>
		</section>

		<!-- DELETE SUBJECT MODAL -->
		<section class="semantic-content" id="modal-delete-subject" tabindex="-1" role="dialog" aria-labelledby="modal-label" aria-hidden="true">

		    <div class="modal-inner">
		        <header id="modal-label"><h2>Delete <?php echo $result['subject_name']; ?></h2></header>
		        <div class="modal-content">
					<form action="" method="POST">
						<div class="field">
							<p>Are You Sure to delete this Subject ?</p>
						</div>
						<div class="clear"></div>
						<input type="hidden" name="subject_code" value="<?php echo $subject_code; ?>">
						<div class="field">
							<input class="button button-success" name="deleteSubjectCommit" type="submit" value="Commit">
						</div>
					</form>
					</div>
		        <footer>
		        	<small>* Please Note that this action not reversible. Continue at your own risk.</small>
		        </footer>
		    </div>

    		<a href="#!" class="modal-close" title="Close this modal" data-close="Close" data-dismiss="modal">×</a>
		</section>


	</div>
</body>
</html>
<?php
}
else
{
	header('HTTP/1.0 403 Forbidden');
	echo '403 Forbidden';
	exit();
}