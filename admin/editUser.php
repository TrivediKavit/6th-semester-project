<?php

require("../includes/admin.inc.php");

if(!isset($_SESSION['user_id']))
{
	header("Location: ../index.php");
	exit();
}

if($_SESSION['userlevel'] == 0)
{
	//FORM HANDLING
	if(isset($_POST) && !empty($_POST))
	{
		if(isset($_POST['studentCommit']))
		{
			if(isset($_SESSION['sid']) && ($_SESSION['sid'] === $_POST['sid']))
			{
				$sid = $_SESSION['sid'];
				updateStudent($sid,$_POST,$DB);
				header("Location: editUser.php?uid={$_GET['uid']}#!");
			}
			else
			{
				echo 'Some Error Occured.';
			}
		}
		else if(isset($_POST['facultyCommit']))
		{
			if(isset($_SESSION['fid']) && ($_SESSION['fid'] === $_POST['fid']))
			{
				$fid = $_SESSION['fid'];
				updateFaculty($fid,$_POST,$DB);
				header("Location: editUser.php?uid={$_GET['uid']}#!");
			}
			else
			{
				echo 'Some Error Occured.';
			}
		}
		else if(isset($_POST['assignSubjectCommit']))
		{
			if(isset($_SESSION['fid']) && ($_SESSION['fid'] === $_POST['fid']))
			{
				$fid = $_SESSION['fid'];
				$subject = $_POST['subject'];
				assignSubjectToFaculty($fid,$subject,$DB);
				header("Location: editUser.php?uid={$_GET['uid']}#!");
			}
			else
			{
				echo 'Some Error Occured.';
			}
		}
		else if(isset($_POST['removeSubjectCommit']))
		{
			if(isset($_SESSION['fid']) && ($_SESSION['fid'] === $_POST['fid']))
			{
				$fid = $_SESSION['fid'];
				$subject = $_POST['subject'];
				removeSubjectFromFaculty($fid,$subject,$DB);
				header("Location: editUser.php?uid={$_GET['uid']}#!");
			}
			else
			{
				echo 'Some Error Occured.';
			}
		}
		else if(isset($_POST['deleteUserCommit']))
		{
			if(isset($_SESSION['uuid']) && ($_SESSION['uuid'] === $_POST['uid']) && ($_POST['uid'] === $_GET['uid']))
			{
				$uid = $_GET['uid'];
				deleteUser($uid,$DB);
				header("Location: manageUsers.php#!");
			}
			else
			{
				echo 'Some Error Occured.';
			}
		}
		else
		{
			header('HTTP/1.0 403 Forbidden');
			echo '403 Forbidden';
		}
		exit();
	}


	if(isset($_GET['uid']) && is_numeric($_GET['uid']))
	{
		$uid = $_GET['uid'];
		$_SESSION['uuid'] = $uid;
	}	
	else
	{
		header("Location: manageUsers.php#!");
		exit();
	}
	$result = getUserByID($uid,$DB);
	$username = $result['username'];
	if($result['userlevel'] == 1)
	{
		$result = getFacultyByUserID($uid,$DB);
		$fid = $result['faculty_id'];
		if(isset($_SESSION['fid']))
		{
			unset($_SESSION['fid']);
		}
		$_SESSION['fid'] = $fid;
		$fn = $result['firstname'];
		$ln = $result['lastname'];
		$dept = $result['dept_code'];
		$result = getSubjectsAssignedToFaculty($fid,$DB);
		$scount = count($result);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/modal.css">
	<title>Edit Faculty Information</title>
</head>
<body>
	<div class="wrapper">
		<!-- HEADER -->
		<div class="header">
			<div class="container">
				<div class="logo">
					<h1><a href="../index.php">Attendance Manager</a></h1>
					<p>Admin Control Panel</p>
				</div>
				<div class="links">
					<ul class="top-links">
						<li><span>Welcome, <?php echo $_SESSION['username'];?></span></li>
						<li><a href="../logout.php" class="button button-primary">Log Out</a></li>
					</ul>
				</div>
			</div>
		</div>

		<!-- CONTENT AREA -->
		<div class="content-wrapper">
			<div class="container">
				<div class="content">
					<div class="sidebar">
						<ul class="side-links">
							<li><a href="manageUsers.php" class="button button-full-width button-side-links">Manage Users</a></li>
							<li><a href="manageSubjects.php" class="button button-full-width button-side-links">Manage Subjects</a></li>
						</ul>
					</div>
					<div class="main-content">
						<div class="personal-details">
							<h2>Faculty Information</h2>
							<div class="details">
								<ul>
									<li><span class="bold">Name: </span><?php echo $fn . ' ' . $ln; ?></li>
									<li><span class="bold">Department: </span><?php echo getDepartment($dept); ?></li>
									<li><span class="bold">Subjects Assigned : </span><?php echo $scount; ?></li>
								</ul>
							</div>
							<div class="image">
								<img src="http://placehold.it/140X100" alt="Placeholder">
							</div>
						</div>
						<div class="clear"></div>
						<div class="assigned-classes">
							<h2>Subjects Assigned</h2>
							<div class="table-large">
								<table> 
									<thead>
										<tr>
											<th>Sr. No</th>
											<th>Subject Code</th>
											<th>Semester</th>
											<th>Subject Name</th>
											<th>Options</th>
										</tr>
									</thead>
									<tbody>
										<?php

											$count=1;
											foreach($result as $s)
											{
												if($count%2 == 0)
												{
													echo '<tr class="odd">';
												}
												else
												{
													echo '<tr class="even">';
												}

												echo '<td>' . $count . '</td>';
												echo '<td>' . $s['subject_code'] . '</td>';
												echo '<td>' . $s['semester'] . '</td>';
												echo '<td>' . $s['subject_name'] . '</td>';
												echo '<td><a href="editUser.php?uid=' . $uid . '&subject=' . $s['subject_code'] . '#modal-confirm">Remove Subject</a></td>';
												echo '</tr>';
												$count++;

											}

										?>
									</tbody>
								</table>
							</div>
						</div>
						<p>+ <a href="#modal-assign-subject1" class="call-modal">Add Subject</a></p>
						<p>
							<a href="#modal-edit-info" class="call-modal">Edit Faculty Information</a> | 
							<a href="#modal-delete-user" class="call-modal">Delete Faculty</a>
						</p>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		
		<!-- FOOTER -->
		<div class="footer">
			<span class="copyright">Copyright </span>
			<span class="year"> 2014</span>
		</div>

		<!-- EDIT FACULTY INFO MODAL -->
		<section class="semantic-content" id="modal-edit-info" tabindex="-1" role="dialog" aria-labelledby="modal-label" aria-hidden="true">

		    <div class="modal-inner">
		        <header id="modal-label"><h2>Edit Faculty Information</h2></header>
		        <div class="modal-content">
		        	<form action="" method="POST">
						<div class="field">
							<label for="firstname">First Name : </label>
							<input type="text" name="firstname" value="<?php echo $fn; ?>">
						</div>
						<div class="clear"></div>
						<div class="field">
							<label for="lastname">Last Name : </label>
							<input type="text" name="lastname" value="<?php echo $ln; ?>">
						</div>
						<div class="clear"></div>
						<div class="field">
							<label for="dept">Department : </label>
							<select name="dept">
								<option disabled>Select a Department</option>
								<option selected value="7">Computer Engineering</option>
							</select>
						</div>
						<input type="hidden" name="fid" value="<?php echo $fid; ?>">
						<div class="clear"></div>
						<div class="field">
							<input class="button button-success" name="facultyCommit" type="submit" value="Commit">
						</div>
					</form>
		        </div>
		        <footer>
		        	<small>* Please Note that this action not reversible. Continue at your own risk.</small>
		        </footer>
		    </div>

    		<a href="#!" class="modal-close" title="Close this modal" data-close="Close" data-dismiss="modal">×</a>
		</section>

		<!-- ASSIGN SUBJECT MODAL ONE-->
		<section class="semantic-content" id="modal-assign-subject1" tabindex="-1" role="dialog" aria-labelledby="modal-label" aria-hidden="true">

		    <div class="modal-inner">
		        <header id="modal-label"><h2>Assign Subject Information for <?php echo $username; ?></h2></header>
		        <div class="modal-content">
		        	<form action="#modal-assign-subject2" method="GET">
						<div class="field">
							<label for="class">Select Semester : </label>
							<select name="sem">
								<?php

									$options = getSemesters();

									foreach($options as $option)
									{
										echo '<option value="'.$option['value'].'">'.$option['option'].'</option>';
									}
								?>
							</select>
						</div>
						<div class="clear"></div>
						<input type="hidden" name="uid" value="<?php echo $uid; ?>">
						<div class="field">
							<input class="button button-success" type="submit" value="Next">
						</div>
					</form>
						
		        </div>
		        <footer>
		        	<small>* Please Note that this action not reversible. Continue at your own risk.</small>
		        </footer>
		    </div>

    		<a href="#!" class="modal-close" title="Close this modal" data-close="Close" data-dismiss="modal">×</a>
		</section>

		<!-- ASSIGN SUBJECT MODAL TWO-->
		<section class="semantic-content" id="modal-assign-subject2" tabindex="-1" role="dialog" aria-labelledby="modal-label" aria-hidden="true">

		    <div class="modal-inner">
		        <header id="modal-label"><h2>Assign Subject Information for <?php echo $username; ?></h2></header>
		        <div class="modal-content">
					<form action="" method="POST">
						<div class="field">
							<label for="subject">Select Subject : </label>
							<select name="subject">
								<?php
									if(isset($_GET['sem']))
									{
										$sem = $_GET['sem'];
										$options = getSubjectsBySemesterAndDepartment($sem,$dept,$DB);

										foreach($options as $option)
										{
											echo '<option value="'.$option['subject_id'].'">'.$option['subject_name'].'</option>';
										}
									}
									else
									{
										echo '<option disabled>Select a Semester</option>';
									}
								?>
							</select>
						</div>
						<div class="clear"></div>
						<input type="hidden" name="fid" value="<?php echo $fid; ?>">
						<div class="field">
							<input class="button button-success" name="assignSubjectCommit" type="submit" value="Commit">
						</div>
					</form>
					</div>
		        <footer>
		        	<small>* Please Note that this action not reversible. Continue at your own risk.</small>
		        </footer>
		    </div>

    		<a href="#!" class="modal-close" title="Close this modal" data-close="Close" data-dismiss="modal">×</a>
		</section>

		<!-- REMOVE SUBJECT MODAL -->
		<?php if(isset($_GET['subject'])){ $subject = $_GET['subject']; ?>
		<section class="semantic-content" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="modal-label" aria-hidden="true">

		    <div class="modal-inner">
		        <header id="modal-label"><h2>Remove Subject Assigned to <?php echo $username; ?></h2></header>
		        <div class="modal-content">
					<form action="" method="POST">
						<div class="field">
							<p>Are You Sure to remove Subject <?php $s = getSubjectByCode($subject,$DB); echo $s['subject_name']; ?> from this faculty?</p>
						</div>
						<div class="clear"></div>
						<input type="hidden" name="fid" value="<?php echo $fid; ?>">
						<input type="hidden" name="subject" value="<?php echo getSubjectIDByCode($subject,$DB); ?>">
						<div class="field">
							<input class="button button-success" name="removeSubjectCommit" type="submit" value="Commit">
						</div>
					</form>
					</div>
		        <footer>
		        	<small>* Please Note that this action not reversible. Continue at your own risk.</small>
		        </footer>
		    </div>

    		<a href="#!" class="modal-close" title="Close this modal" data-close="Close" data-dismiss="modal">×</a>
		</section>
		<?php } ?>

				<!-- DELETE USER MODAL -->
		<section class="semantic-content" id="modal-delete-user" tabindex="-1" role="dialog" aria-labelledby="modal-label" aria-hidden="true">

		    <div class="modal-inner">
		        <header id="modal-label"><h2>Delete <?php echo $username; ?></h2></header>
		        <div class="modal-content">
					<form action="" method="POST">
						<div class="field">
							<p>Are You Sure to delete this User ?</p>
						</div>
						<div class="clear"></div>
						<input type="hidden" name="uid" value="<?php echo $uid; ?>">
						<div class="field">
							<input class="button button-success" name="deleteUserCommit" type="submit" value="Commit">
						</div>
					</form>
					</div>
		        <footer>
		        	<small>* Please Note that this action not reversible. Continue at your own risk.</small>
		        </footer>
		    </div>

    		<a href="#!" class="modal-close" title="Close this modal" data-close="Close" data-dismiss="modal">×</a>
		</section>


	</div>
</body>
</html>
<?php

	}
	else
	{
		$result = getStudentByUserID($uid,$DB);
		$sid = $result['student_id'];
		if(isset($_SESSION['sid']))
		{
			unset($_SESSION['sid']);
		}
		$_SESSION['sid'] = $sid;
		$fn = $result['firstname'];
		$ln = $result['lastname'];
		$enrollment_number = $result['enrollment_number'];
		$sem = $result['semester'];
		$dept_code = $result['dept_code'];
		$result = getSubjectsBySemesterAndDepartment($sem,$dept_code,$DB);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/modal.css">
	<title>Edit Student Information</title>
</head>
<body>
	<div class="wrapper">
		<!-- HEADER -->
		<div class="header">
			<div class="container">
				<div class="logo">
					<h1><a href="../index.php">Attendance Manager</a></h1>
					<p>Admin Control Panel</p>
				</div>
				<div class="links">
					<ul class="top-links">
						<li><span>Welcome, <?php echo $_SESSION['username'];?></span></li>
						<li><a href="../logout.php" class="button button-primary">Log Out</a></li>
					</ul>
				</div>
			</div>
		</div>

		<!-- CONTENT AREA -->
		<div class="content-wrapper">
			<div class="container">
				<div class="content">
					<div class="sidebar">
						<ul class="side-links">
							<li><a href="manageUsers.php" class="button button-full-width button-side-links">Manage Users</a></li>
							<li><a href="manageSubjects.php" class="button button-full-width button-side-links">Manage Subjects</a></li>
						</ul>
					</div>
					<div class="main-content">
						<div class="personal-details">
							<h2>Student Information</h2>
							<div class="details">
								<ul>
									<li><span class="bold">Name: </span><?php echo $fn . ' ' . $ln; ?></li>
									<li><span class="bold">Enrollment Number: </span><?php echo $enrollment_number; ?></li>
									<li><span class="bold">Semester: </span><?php echo $sem; ?></li>
									<li><span class="bold">Department: </span><?php echo getDepartment($dept_code); ?></li>
								</ul>
							</div>
							<div class="image">
								<img src="http://placehold.it/140X100" alt="Placeholder">
							</div>
						</div>
						<div class="clear"></div>
						<div class="assigned-classes">
							<h2>Subjects</h2>
							<div class="table-large">
								<table> 
									<thead>
										<tr>
											<th>Sr. No</th>
											<th>Subject Code</th>
											<th>Subject Name</th>
											<th>Options</th>
										</tr>
									</thead>
									<tbody>
										<?php

											$count=1;
											foreach($result as $s)
											{
												if($count%2 == 0)
												{
													echo '<tr class="odd">';
												}
												else
												{
													echo '<tr class="even">';
												}

												echo '<td>' . $count . '</td>';
												echo '<td>' . $s['subject_code'] . '</td>';
												echo '<td>' . $s['subject_name'] . '</td>';
												echo '<td><a href="../check.php?subject=' . $s['subject_code'] . '">Check Attendance</a></td>';
												echo '</tr>';
												$count++;

											}

										?>
									</tbody>
								</table>
							</div>
						</div>
						<p>
							<a href="#modal-edit-info" class="call-modal">Edit Student Information</a> | 
							<a href="#modal-delete-user" class="call-modal">Delete Student</a>
						</p>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		
		<!-- FOOTER -->
		<div class="footer">
			<span class="copyright">Copyright </span>
			<span class="year"> 2014</span>
		</div>

		<!-- EDIT STUDENT INFO MODAL -->
		<section class="semantic-content" id="modal-edit-info" tabindex="-1" role="dialog" aria-labelledby="modal-label" aria-hidden="true">

		    <div class="modal-inner">
		        <header id="modal-label"><h2>Edit Student Information</h2></header>
		        <div class="modal-content">
		        	<form action="" method="POST">
		        		<div class="field">
							<label for="en">Enrollment Number : </label>
							<input type="text" name="en" value="<?php echo $enrollment_number; ?>">
						</div>
						<div class="field">
							<label for="firstname">First Name : </label>
							<input type="text" name="firstname" value="<?php echo $fn; ?>">
						</div>
						<div class="clear"></div>
						<div class="field">
							<label for="lastname">Last Name : </label>
							<input type="text" name="lastname" value="<?php echo $ln; ?>">
						</div>
						<div class="clear"></div>
						<div class="field">
							<label for="dept">Department : </label>
							<select name="dept">
								<option disabled>Select a Department</option>
								<option selected value="7">Computer Engineering</option>
							</select>
						</div>
						<div class="clear"></div>
						<div class="field">
							<label for="semester">Semester : </label>
							<select name="semester">
								<?php

									$options = getSemesters();

									foreach($options as $option)
									{
										if($option['value'] == $sem)
										{
											echo '<option selected value="'.$option['value'].'">'.$option['option'].'</option>';
										}
										else
										{
											echo '<option value="'.$option['value'].'">'.$option['option'].'</option>';
										}
									}
								?>
							</select>
						</div>
						<div class="clear"></div>
						<input type="hidden" name="sid" value="<?php echo $sid; ?>">
						<div class="field">
							<input class="button button-success" name="studentCommit" type="submit" value="Commit">
						</div>
					</form>
		        </div>
		        <footer>
		        	<small>* Please Note that this action not reversible. Continue at your own risk.</small>
		        </footer>
		    </div>

    		<a href="#!" class="modal-close" title="Close this modal" data-close="Close" data-dismiss="modal">×</a>
		</section>

		<!-- DELETE USER MODAL -->
		<section class="semantic-content" id="modal-delete-user" tabindex="-1" role="dialog" aria-labelledby="modal-label" aria-hidden="true">

		    <div class="modal-inner">
		        <header id="modal-label"><h2>Delete <?php echo $username; ?></h2></header>
		        <div class="modal-content">
					<form action="" method="POST">
						<div class="field">
							<p>Are You Sure to delete this User ?</p>
						</div>
						<div class="clear"></div>
						<input type="hidden" name="uid" value="<?php echo $uid; ?>">
						<div class="field">
							<input class="button button-success" name="deleteUserCommit" type="submit" value="Commit">
						</div>
					</form>
					</div>
		        <footer>
		        	<small>* Please Note that this action not reversible. Continue at your own risk.</small>
		        </footer>
		    </div>

    		<a href="#!" class="modal-close" title="Close this modal" data-close="Close" data-dismiss="modal">×</a>
		</section>


	</div>
</body>
</html>
<?php
	}

}
else
{
	header('HTTP/1.0 403 Forbidden');
	echo '403 Forbidden';
	exit();
}