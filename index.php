<?php

require("includes/init.inc.php");

if(!isset($_SESSION['user_id']))
{
	header("Location: login.php");
	exit();
}
else
{
	header("Location: profile.php");
	exit();
}