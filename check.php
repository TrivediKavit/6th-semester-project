<?php

require("includes/init.inc.php");

if(!isset($_SESSION['user_id']))
{
	header("Location: login.php");
	exit();
}

if(($_SESSION['userlevel'] == 2) || ($_SESSION['userlevel'] == 0 && isset($_SESSION['sid'])))
{
	if($_SESSION['userlevel'] == 2)
	{
		$uid = $_SESSION['user_id'];
		$result = getStudentByUserID($uid,$DB);
		$sid = $result['student_id'];
	}
	else
	{
		$sid = $_SESSION['sid'];
		$result = getStudentByStudentID($sid,$DB);
	}
	$fn = $result['firstname'];
	$ln = $result['lastname'];
	$dept = $result['dept_code'];
	$sem = $result['semester'];

	if(isset($_GET['subject']))
	{
		$s = $_GET['subject'];
		$subject = getSubjectByCode($s,$DB);
		$arecords= getLatestAttendance($sid,$s,$DB);

		$fid = isset($arecords[0]['faculty_id']) ? $arecords[0]['faculty_id'] : NULL;
		if($fid == NULL)
		{
			echo "Data Not Available. Please Check Back Later.";
			exit();
		}
		$attended_lectures = getAttendedLectures($sid,$s,$DB);
		$total_lectures = getTotalLectures($s,$fid,$DB);
		$percentage = ($attended_lectures/$total_lectures)*100;
		$present = number_format($percentage, 2, '.', ',') . '%';
		$absent = number_format(100-$percentage, 2, '.', ',') . '%';
		$print = "<div class='graph'>
					<div class='record'><div class='bar' style='width:{$present};'><span>{$present}</span></div><div class='p'><span>{$absent}</span></div></div>
					<div class='clear'></div>
					<div class='base'></div>
				</div>
				 ";

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/style.css">
	<title>Check Attendance</title>
</head>
<body>
	<div class="wrapper">
		<!-- HEADER -->
		<div class="header">
			<div class="container">
				<div class="logo">
					<h1><a href="index.php">Attendance Manager</a></h1>
					<p>Check Attendance</p>
				</div>
				<div class="links">
					<ul class="top-links">
						<li><span>Welcome, <?php echo $_SESSION['username'];?></span></li>
						<li><a href="logout.php" class="button button-primary">Log Out</a></li>
					</ul>
				</div>
			</div>
		</div>

		<!-- CONTENT AREA -->
		<div class="content-wrapper">
			<div class="container">
				<div class="content">
					<div class="sidebar">
						<ul class="side-links">
							<?php if($_SESSION['userlevel'] == 0) { ?>
							<li><a href="admin/manageUsers.php" class="button button-full-width button-side-links">Manage Users</a></li>
							<li><a href="admin/manageSubjects.php" class="button button-full-width button-side-links">Manage Subjects</a></li>
							<?php }else{ ?>
							<li><a href="check.php" class="button button-full-width button-side-links">Check Attendance</a></li>
							<li><a href="#" class="button button-full-width button-side-links">Edit Profile</a></li>
							<?php } ?>
						</ul>
					</div>
					<div class="main-content">
						<div class="personal-details">
							<h2>Attendance</h2>
							<div class="details">
								<ul>
									<li><span class="bold">Student Name : </span><?php echo $fn . ' ' . $ln; ?></li>
									<li><span class="bold">Department : </span><?php echo getDepartment($dept); ?></li>
									<li><span class="bold">Subject Name : </span><?php echo $subject['subject_name']; ?></li>
									<li><span class="bold">Total Lectures : </span><?php echo $total_lectures; ?></li>
									<li><span class="bold">Attended Lectures : </span><?php echo $attended_lectures; ?></li>
									<li><span class="bold">Present in : </span><?php echo $present; ?> of Lectures</li>
								</ul>
							</div>
							<div class="image">
								<img src="http://placehold.it/140X100" alt="Placeholder">
							</div>
						</div>
						<div class="clear"></div>
						<div class="assigned-classes">
							<h2>Status</h2>
							<div class="graph">
								<div class="record">
									<div class="bar" style="width:<?php echo $present; ?>;"><span><?php echo $present; ?></span></div>
									<?php if($percentage<99) { ?>
									<div class="p"><span><?php echo $absent; ?></span></div>
									<?php } ?>

								</div>
								<div class="clear"></div>
								<div class="base"></div>
							</div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		
		<!-- FOOTER -->
		<div class="footer">
			<span class="copyright">Copyright </span>
			<span class="year"> 2014</span>
		</div>
	</div>
</body>
</html>
<?php

	}
	else
	{
		$result = getSubjectsBySemesterAndDepartment($sem,$dept,$DB);

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/style.css">
	<title>Check Attendance</title>
</head>
<body>
	<div class="wrapper">
		<!-- HEADER -->
		<div class="header">
			<div class="container">
				<div class="logo">
					<h1><a href="#">Attendance Manager</a></h1>
					<p>Check Attendance</p>
				</div>
				<div class="links">
					<ul class="top-links">
						<li><span>Welcome, <?php echo $_SESSION['username'];?></span></li>
						<li><a href="logout.php" class="button button-primary">Log Out</a></li>
					</ul>
				</div>
			</div>
		</div>

		<!-- CONTENT AREA -->
		<div class="content-wrapper">
			<div class="container">
				<div class="content">
					<div class="sidebar">
						<ul class="side-links">
							<?php if($_SESSION['userlevel'] == 0) { ?>
							<li><a href="admin/manageUsers.php" class="button button-full-width button-side-links">Manage Users</a></li>
							<li><a href="admin/manageSubjects.php" class="button button-full-width button-side-links">Manage Subjects</a></li>
							<?php }else{ ?>
							<li><a href="check.php" class="button button-full-width button-side-links">Check Attendance</a></li>
							<li><a href="#" class="button button-full-width button-side-links">Edit Profile</a></li>
							<?php } ?>
						</ul>
					</div>
					<div class="main-content">
						<p>Please select a subject from the table.</p>
						<div class="assigned-classes">
							<h2>Classes Taken</h2>
							<div class="table-large">
								<table> 
									<thead>
										<tr>
											<th>Sr. No</th>
											<th>Subject Code</th>
											<th>Semester</th>
											<th>Subject</th>
											<th>Options</th>
										</tr>
									</thead>
									<tbody>
										<?php

											$count=1;
											foreach($result as $s)
											{
												if($count%2 == 0)
												{
													echo '<tr class="odd">';
												}
												else
												{
													echo '<tr class="even">';
												}

												echo '<td>' . $count . '</td>';
												echo '<td>' . $s['subject_code'] . '</td>';
												echo '<td>' . $s['semester'] . '</td>';
												echo '<td>' . $s['subject_name'] . '</td>';
												echo '<td><a href="check.php?subject=' . $s['subject_code'] . '">Check Attendance</a></td>';
												echo '</tr>';
												$count++;

											}

										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		
		<!-- FOOTER -->
		<div class="footer">
			<span class="copyright">Copyright </span>
			<span class="year"> 2014</span>
		</div>
	</div>
</body>
</html>
<?php
	}

}

else
{
	header('HTTP/1.0 403 Forbidden');
	echo '403 Forbidden';
	exit();
}