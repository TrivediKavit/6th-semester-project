<?php

require("includes/init.inc.php");

if(!isset($_SESSION['user_id']))
{
	if(isset($_POST['username']) && isset($_POST['password']))
	{
		if(!empty($_POST['username']) && !empty($_POST['password']))
		{
			$u = $_POST['username'];
			$p = $_POST['password'];

			$result = getUserByUsername($u,$DB);
			if($result['password'] === $_POST['password'])
			{
				$_SESSION['user_id'] = $result['user_id'];
				$_SESSION['username'] = $result['username'];
				$_SESSION['userlevel'] = $result['userlevel'];
				header("Location: profile.php");
				exit();
			}
			else
			{
				echo 'Some Problem Occured';
			}
		}
		else
		{
			echo 'All the fields are required.';
		}
	}
}
else
{
	header("Location: profile.php");
	exit();
}



?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/style.css">
	<title>Log In</title>
</head>
<body>
	<div class="wrapper">

		<!-- LOGIN FORM -->
		<div class="login-header">
			<div class="logo">
				<h1><a href="#">Attendance Manager</a></h1>
				<p>Log In</p>
			</div>
		</div>
		<div class="login-form">
			<form action="" method="post">
				<div class="field">
					<input type="text" name="username" placeholder="Username">
				</div>
				<div class="field">
					<input type="password" name="password" placeholder="Password">
				</div>
				<div class="field">
					<input class="button button-success" type="submit" value="Log In">
					<input class="button button-primary" type="reset"  value="Clear">
				</div>
			</form>
		</div>

		<!-- FOOTER -->
		<div class="footer">
			<span class="copyright">Copyright </span>
			<span class="year"> 2014</span>
		</div>
	</div>
</body>
</html>