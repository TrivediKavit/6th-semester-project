<?php

function getAllUsers($DB)
{
	$query = $DB->query("SELECT *
				 		 FROM  `users`
				 		 WHERE `userlevel` != 0");
	$result = $query->fetchAll(PDO::FETCH_ASSOC);
	return $result;
}

function getUserByUsername($username,$DB)
{
	$query = $DB->query("SELECT *
				 		 FROM  `users`
				 		 WHERE `username` = '{$username}'");
	$result = $query->fetch(PDO::FETCH_ASSOC);
	return $result;
}

function getUserByID($id,$DB)
{
	$query = $DB->query("SELECT *
				 		 FROM  `users`
				 		 WHERE `user_id` = {$id}");
	$result = $query->fetch(PDO::FETCH_ASSOC);
	return $result;
}

function getUsersByLevel($level,$DB)
{
	$query = $DB->query("SELECT *
				 		 FROM  `users`
				 		 WHERE `userlevel` = {$level}");
	$result = $query->fetchAll(PDO::FETCH_ASSOC);
	return $result;
}

function getLatestUsers($DB)
{
	$query = $DB->query("SELECT *
				 		 FROM  `users`
				 		 WHERE `userlevel` != 0 
				 		 ORDER BY `user_id` DESC
				 		 LIMIT 0,5");
	$result = $query->fetchAll(PDO::FETCH_ASSOC);
	return $result;
}