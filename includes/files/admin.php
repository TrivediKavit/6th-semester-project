<?php

function updateStudent($sid,$array,$DB)
{
	$fn 	= 	$array['firstname'];
	$ln 	= 	$array['lastname'];
	$sem 	=	$array['semester'];
	$dept 	=	$array['dept'];
	$en 	=	$array['en'];

	$query = $DB->query("UPDATE `students`
				 		 SET `enrollment_number` 	= 	{$en},
				 		 	 `firstname` 			= 	'{$fn}',
				 		 	 `lastname`  			= 	'{$ln}',
				 		 	 `semester`  			= 	{$sem},
				 		 	 `dept_code` 			= 	{$dept}
				 		 WHERE `student_id` = {$sid}");
}

function updateFaculty($fid,$array,$DB)
{
	$fn 	= 	$array['firstname'];
	$ln 	= 	$array['lastname'];
	$dept 	=	$array['dept'];

	$query = $DB->query("UPDATE `faculty`
				 		 SET 	`firstname` 	= 	'{$fn}',
				 		 	 	`lastname`  	= 	'{$ln}',
				 		 	 	`dept_code` 	= 	{$dept}
				 		 WHERE 	`faculty_id` 	= 	{$fid}");
}

function assignSubjectToFaculty($fid,$subject,$DB)
{
	$query = $DB->query("SELECT * 
						FROM 	`map_faculty_subject_semester`
						WHERE	`subject_id` = {$subject}");

	if($query->rowCount() == 0)
	{
		$query = $DB->query("SELECT * 
						FROM 	`map_faculty_subject_semester`
						WHERE	`faculty_id` = {$fid}
						AND		`subject_id` = {$subject}");
	}

	if($query->rowCount() == 0)
	{
		$query = $DB->query("INSERT INTO `map_faculty_subject_semester`(`faculty_id`,`subject_id`)
								VALUES ({$fid},{$subject})");
	}
	
}

function removeSubjectFromFaculty($fid,$subject,$DB)
{
	$query = $DB->query("DELETE FROM 	`map_faculty_subject_semester`
						WHERE 	`faculty_id` = {$fid}
						AND		`subject_id` = {$subject}");
}

function deleteUser($uid,$DB)
{

	$result = getUserByID($uid,$DB);
	if($result['userlevel'] == 1)
	{
		$table = 'faculty';
	}
	else
	{
		$table = 'students';
	}
	$query = $DB->query("DELETE FROM 	`{$table}`
						WHERE 			`user_id` = {$uid}");

	$query = $DB->query("DELETE FROM 	`users`
						WHERE 			`user_id` = {$uid}");
}

function createUser($array,$DB)
{
	$username 	= 	$array['username'];
	$password	= 	$array['password'];
	$email 		= 	$array['email'];
	$userlevel 	= 	$array['userlevel'];

	$query = $DB->query("INSERT INTO `users`(`username`,`password`,`email`,`userlevel`)
							VALUES ('{$username}','{$password}','{$email}',{$userlevel})");

	$result = $DB->lastInsertId('user_id');

	sendRegistrationEmail($username,$password,$email);

	return $result;
}

function createFaculty($uid,$array,$DB)
{
	$firstname 	= 	$array['firstname'];
	$lastname	= 	$array['lastname'];
	$dept_code 	= 	$array['dept_code'];

	$query = $DB->query("INSERT INTO `faculty`(`user_id`,`firstname`,`lastname`,`dept_code`)
							VALUES ({$uid},'{$firstname}','{$lastname}',{$dept_code})");
}

function createStudent($uid,$array,$DB)
{
	$firstname 			= 	$array['firstname'];
	$lastname			= 	$array['lastname'];
	$enrollment_number	= 	$array['enrollment_number'];
	$semester			= 	$array['semester'];
	$dept_code 			= 	$array['dept_code'];

	$query = $DB->query("INSERT INTO `students`(`user_id`,`firstname`,`lastname`,`enrollment_number`,`semester`,`dept_code`)
							VALUES ({$uid},'{$firstname}','{$lastname}',{$enrollment_number},{$semester},{$dept_code})");

}

function createSubject($array,$DB)
{
	$subject_code 		= 	$array['subject_code'];
	$subject_name		= 	$array['subject_name'];
	$subject_credits	= 	$array['subject_credits'];
	$semester			= 	$array['semester'];
	$dept_code 			= 	$array['dept_code'];

	$query = $DB->query("INSERT INTO `subjects`(`subject_code`,`subject_name`,`subject_credits`,`semester`,`dept_code`)
							VALUES ({$subject_code},'{$subject_name}',{$subject_credits},{$semester},{$dept_code})");

}

function updateSubject($subject_code,$array,$DB)
{

	$subject 			= 	getSubjectByCode($subject_code,$DB);
	$subject_id 		= 	$subject['subject_id'];
	
	$subject_name		= 	$array['subject_name'];
	$subject_credits	= 	$array['subject_credits'];
	$semester			= 	$array['semester'];
	$dept_code 			= 	$array['dept_code'];

	$query = $DB->query("UPDATE `subjects`
						SET		`subject_name`  	= 	'{$subject_name}',
				 		 	 	`subject_credits` 	= 	{$subject_credits},
				 		 	 	`semester`			=	{$semester},
				 		 	 	`dept_code` 		= 	{$dept_code}
				 		WHERE 	`subject_id` 		= 	{$subject_id}");
}

function deleteSubject($subject_code,$DB)
{

	$subject = getSubjectByCode($subject_code,$DB);
	$subject_id = $subject['subject_id'];

	$query = $DB->query("DELETE FROM 	`map_faculty_subject_semester`
						WHERE 	`subject_id` = {$subject_id}");

	$query = $DB->query("DELETE FROM 	`subjects`
						WHERE 	`subject_id` = {$subject_id}");
}

function sendRegistrationEmail($username,$password,$email)
{
	$mail = new PHPMailer(true);
	$mail->IsSMTP();

	$body = "Hello,<br /><br />

			Your Account has been created by the Administrator using this email address.<br /><br />

			Please Login using following credentials<br /><br />

			Username : {$username}<br />
			Password : {$password}<br /><br />

			You can login at <a href=\"http://kntfixes.tk/attendance-manager/login.php\">http://kntfixes.tk/attendance-manager/login.php</a><br /><br /><br />

			For any queries, just send an email to <a href=\"mailto:contact@kntfixes.tk\">contact@kntfixes.tk</a>";

	try
	{
		$mail->Host       = "kntfixes.tk";
		$mail->SMTPAuth   = true;
		$mail->SMTPSecure = "ssl";
		$mail->Host       = "smtp.gmail.com";
		$mail->Port       = 465;
		$mail->Username   = GMAIL_USERNAME;
		$mail->Password   = GMAIL_PASSWORD;
		$mail->AddAddress("{$email}", "{$username}");
		$mail->SetFrom('donotreply@kntfixes.tk', 'AUTO - Attendance Manager');
		$mail->AddReplyTo('donotreply@kntfixes.tk', 'AUTO - Attendance Manager');
		$mail->Subject = 'Your Account has been created - Attendance Manager';
		$mail->MsgHTML($body);
		$mail->Send();
	}
	catch (phpmailerException $e)
	{
		echo $e->errorMessage();
	}
	catch (Exception $e)
	{
		echo $e->getMessage();
	}
}