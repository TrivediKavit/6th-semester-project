<?php

function getAllSubjects($DB)
{
	$query = $DB->query("SELECT *
				 		 FROM  `subjects`");
	$result = $query->fetchAll(PDO::FETCH_ASSOC);
	return $result;
}

function getSubjectsBySemesterAndDepartment($sem,$dept,$DB)
{
	$query = $DB->query("SELECT *
				 		 FROM  	`subjects`
				 		 WHERE 	`semester` 	= {$sem} 
				 		 AND (`dept_code` = {$dept} OR `dept_code` = 0);");
	$result = $query->fetchAll(PDO::FETCH_ASSOC);
	return $result;
}

function getSubjectByID($id,$DB)
{
	$query = $DB->query("SELECT *
				 		 FROM  `subjects`
				 		 WHERE `subject_id` = {$id}");
	$result = $query->fetch(PDO::FETCH_ASSOC);
	return $result;
}

function getSubjectByCode($code,$DB)
{
	$query = $DB->query("SELECT *
				 		 FROM  `subjects`
				 		 WHERE `subject_code` = {$code}");
	$result = $query->fetch(PDO::FETCH_ASSOC);
	return $result;
}

function getSubjectCodeByID($id,$DB)
{
	$query = $DB->query("SELECT `subject_code`
				 		 FROM  `subjects`
				 		 WHERE `subject_id` = {$id}");
	$result = $query->fetch(PDO::FETCH_ASSOC);
	return $result['subject_code'];
}

function getSubjectIDByCode($code,$DB)
{
	$query = $DB->query("SELECT `subject_id`
				 		 FROM  `subjects`
				 		 WHERE `subject_code` = {$code}");
	$result = $query->fetch(PDO::FETCH_ASSOC);
	return $result['subject_id'];
}

function getLatestSubjects($DB)
{
	$query = $DB->query("SELECT *
				 		 FROM  `subjects`
				 		 ORDER BY `subject_id` DESC 
				 		 LIMIT 0,5");
	$result = $query->fetchAll(PDO::FETCH_ASSOC);
	return $result;
}

function getDepartment($dept_code)
{
	$result = "GEN";
	switch($dept_code)
	{
		case '7' : $result = "CE";
					break;
	}
	return $result;
}