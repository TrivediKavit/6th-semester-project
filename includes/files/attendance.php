<?php

function getAttendanceByStudentAndSubjectCode($sid,$subject,$DB)
{
	$query = $DB->query("SELECT 	`attendance_detail`.`status`,`attendance_master`.`datetime`,`map_faculty_subject_semester`.`faculty_id`
					 	FROM 		`attendance_detail`, `attendance_master`, `subjects`,`map_faculty_subject_semester`
						WHERE		`attendance_detail`.`attendance_master_id` = `attendance_master`.`attendance_master_id`
						AND		`attendance_detail`.`student_id` = {$sid}
						AND		`attendance_master`.`map_id` = `map_faculty_subject_semester`.`map_id`
						AND		`map_faculty_subject_semester`.`subject_id` = `subjects`.`subject_id`
						AND		`subjects`.`subject_code` = {$subject};");
	$result = $query->fetchAll(PDO::FETCH_ASSOC);
	return $result;
}

function getLatestAttendance($sid,$subject,$DB)
{
	$query = $DB->query("SELECT 	`attendance_detail`.`status`,`attendance_master`.`datetime`,`map_faculty_subject_semester`.`faculty_id`
					 	FROM 		`attendance_detail`, `attendance_master`, `subjects`,`map_faculty_subject_semester`
						WHERE		`attendance_detail`.`attendance_master_id` = `attendance_master`.`attendance_master_id`
						AND		`attendance_detail`.`student_id` = {$sid}
						AND		`attendance_master`.`map_id` = `map_faculty_subject_semester`.`map_id`
						AND		`map_faculty_subject_semester`.`subject_id` = `subjects`.`subject_id`
						AND		`subjects`.`subject_code` = {$subject};
						ORDER BY `attendance_master`.`datetime` DESC
						LIMIT 0,5");
	$result = $query->fetchAll(PDO::FETCH_ASSOC);
	return $result;
}

function getTotalLectures($subject,$fid,$DB)
{
	$query = $DB->query("SELECT `attendance_master`.`attendance_master_id`
					 	FROM 	`attendance_master`, `subjects`,`map_faculty_subject_semester`
						WHERE	`attendance_master`.`map_id` = `map_faculty_subject_semester`.`map_id`
						AND		`map_faculty_subject_semester`.`subject_id` = `subjects`.`subject_id`
						AND		`map_faculty_subject_semester`.`faculty_id` = {$fid}
						AND		`subjects`.`subject_code` = {$subject};");
	$result = $query->rowCount();
	return $result;
}

function getAttendedLectures($sid,$subject,$DB)
{
	$query = $DB->query("SELECT	`attendance_detail`.`attendance_detail_id`
					 	FROM 	`attendance_detail`, `attendance_master`, `subjects`,`map_faculty_subject_semester`
						WHERE	`attendance_detail`.`attendance_master_id` = `attendance_master`.`attendance_master_id`
						AND		`attendance_detail`.`student_id` = {$sid}
						AND		`attendance_detail`.`status` = 1
						AND		`attendance_master`.`map_id` = `map_faculty_subject_semester`.`map_id`
						AND		`map_faculty_subject_semester`.`subject_id` = `subjects`.`subject_id`
						AND		`subjects`.`subject_code` = {$subject};");
	$result = $query->rowCount();
	return $result;
}

function getMapID($fid,$subject,$DB)
{
	$query = $DB->query("SELECT	`map_faculty_subject_semester`.`map_id`
					 	FROM 	`map_faculty_subject_semester`, `subjects`
						WHERE	`map_faculty_subject_semester`.`faculty_id` = {$fid}
						AND		`map_faculty_subject_semester`.`subject_id` = `subjects`.`subject_id`
						AND 	`subjects`.`subject_code` = {$subject};");
	$result = $query->fetch(PDO::FETCH_ASSOC);
	return $result['map_id'];
}

function setAttendanceMaster($fid,$scode,$DB)
{
	$map_id = getMapID($fid,$scode,$DB);
	$datetime = new DateTime('',new DateTimeZone('Asia/Kolkata'));
	$datetime = $datetime->format('Y-m-d H:i:s');

	$query = $DB->query("INSERT INTO `attendance_master` (`map_id`,`datetime`) 
						VALUES ({$map_id},'{$datetime}');");
	$result = $DB->lastInsertId('attendance_master_id');
	return $result;
}

function setAttendanceDetail($master_id,$array,$DB)
{
	$count = count($array);
	$qs = "";
	for($i=0;$i<$count-1;$i++)
	{
		$qs .= "({$master_id},{$array[$i]},1),";
	}
	$qs .= "({$master_id},{$array[$i]},1)";
	$query = $DB->query("INSERT INTO `attendance_detail` (`attendance_master_id`,`student_id`,`status`) VALUES {$qs};");
}