<?php

function getFacultyByUserID($uid,$DB)
{
	$query = $DB->query("SELECT * FROM `faculty` WHERE `user_id`={$uid}");
	$result = $query->fetch(PDO::FETCH_ASSOC);
	return $result;
}

function getFacultyByFacultyID($fid,$DB)
{
	$query = $DB->query("SELECT * FROM `faculty` WHERE `faculty_id`={$fid}");
	$result = $query->fetch(PDO::FETCH_ASSOC);
	return $result;
}

function getFacultyByUsername($uname,$DB)
{
	$query = $DB->query("SELECT `faculty`.* FROM `faculty`,`users`
						 WHERE 	`faculty`.`user_id`=`users`.`user_id`
						 AND	`users`.`username` = '{$uname}';");
	$result = $query->fetch(PDO::FETCH_ASSOC);
	return $result;
}

function getFacultyIDByUserID($uid,$DB)
{
	$query = $DB->query("SELECT `faculty_id` FROM `faculty` WHERE `user_id`={$uid}");
	$result = $query->fetch(PDO::FETCH_ASSOC);
	return $result['Faculty_id'];
}

function getUserIDByFacultyID($fid,$DB)
{
	$query = $DB->query("SELECT `user_id` FROM `faculty` WHERE `faculty_id`={$fid}");
	$result = $query->fetch(PDO::FETCH_ASSOC);
	return $result['user_id'];
}

function getFacultyByDepartment($dept,$DB)
{
	$query = $DB->query("SELECT * FROM `faculty` WHERE `dept_code`={$dept}");
	$result = $query->fetchAll(PDO::FETCH_ASSOC);
	return $result;
}

function getSubjectsAssignedToFaculty($fid,$DB)
{
	$query = $DB->query("SELECT `subjects`.* FROM `faculty`,`subjects`,`map_faculty_subject_semester`
						WHERE 	`faculty`.`faculty_id` = {$fid}
						AND		`faculty`.`dept_code`=`subjects`.`dept_code`
						AND 	`map_faculty_subject_semester`.`faculty_id` = `faculty`.`faculty_id`
						AND		`subjects`.`subject_id` = `map_faculty_subject_semester`.`subject_id`");
	$result = $query->fetchAll(PDO::FETCH_ASSOC);
	return $result;
}