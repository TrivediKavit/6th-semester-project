<?php

function getStudentByUserID($uid,$DB)
{
	$query = $DB->query("SELECT * FROM `students` WHERE `user_id`={$uid}");
	$result = $query->fetch(PDO::FETCH_ASSOC);
	return $result;
}

function getStudentByStudentID($sid,$DB)
{
	$query = $DB->query("SELECT * FROM `students` WHERE `student_id`={$sid}");
	$result = $query->fetch(PDO::FETCH_ASSOC);
	return $result;
}

function getStudentIDByUserID($uid,$DB)
{
	$query = $DB->query("SELECT `student_id` FROM `students` WHERE `user_id`={$uid}");
	$result = $query->fetch(PDO::FETCH_ASSOC);
	return $result['student_id'];
}

function getUserIDByStudentID($sid,$DB)
{
	$query = $DB->query("SELECT `user_id` FROM `students` WHERE `student_id`={$sid}");
	$result = $query->fetch(PDO::FETCH_ASSOC);
	return $result['user_id'];
}

function getStudentsBySemester($sem,$DB)
{
	$query = $DB->query("SELECT * FROM `students` WHERE `semester`={$sem}");
	$result = $query->fetchAll(PDO::FETCH_ASSOC);
	return $result;
}

function getStudentsByDepartment($dept,$DB)
{
	$query = $DB->query("SELECT * FROM `students` WHERE `dept_code`={$dept}");
	$result = $query->fetchAll(PDO::FETCH_ASSOC);
	return $result;
}

function getStudentsBySemesterAndDepartment($sem,$dept,$DB)
{
	$query = $DB->query("SELECT * FROM `students` WHERE `semester`={$sem} AND `dept_code`={$dept} ORDER BY `enrollment_number`");
	$result = $query->fetchAll(PDO::FETCH_ASSOC);
	return $result;
}

function getSemesters()
{
	$result = array(
					array('value'=>1,'option'=>'First'),
					array('value'=>2,'option'=>'Second'),
					array('value'=>3,'option'=>'Third'),
					array('value'=>4,'option'=>'Fourth'),
					array('value'=>5,'option'=>'Fifth'),
					array('value'=>6,'option'=>'Sixth'),
					array('value'=>7,'option'=>'Seventh'),
					array('value'=>8,'option'=>'Eighth')
				);
	return $result;
}