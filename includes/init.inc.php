<?php

session_start();

require_once("/includes/config.php");

$DB = new PDO(DB_TYPE.':host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASS);

require_once("/includes/files/subjects.php");
require_once("/includes/files/users.php");
require_once("/includes/files/attendance.php");
require_once("/includes/files/students.php");
require_once("/includes/files/faculty.php");