<?php

require("includes/init.inc.php");

if(!isset($_SESSION['user_id']))
{
	header("Location: login.php");
	exit();
}

if($_SESSION['userlevel'] == 1)
{
	$uid = $_SESSION['user_id'];
	$result = getFacultyByUserID($uid,$DB);
	$fid = $result['faculty_id'];
	$fn = $result['firstname'];
	$ln = $result['lastname'];
	$dept = $result['dept_code'];

	$subject = null;

	if(isset($_GET['subject']))
	{
		$scode = $_GET['subject'];
	}
	else
	{
		echo "Select from Subject Table on your Profile Page.<br />";
		echo '<a href="profile.php">Click Here to Go Back</a>';
		exit();
	}

	if(isset($_POST) && !empty($_POST))
	{
		if(isset($_SESSION['master_id']))
		{
			$master_id = $_SESSION['master_id'];
			setAttendanceDetail($master_id,$_POST['cb'],$DB);
			unset($_SESSION['master_id']);
			header("Location: ./profile.php");
			exit();
		}
		else
		{
			header("Location: ./take.php?subject={$scode}");
			exit();
		}
	}
	else
	{
		$_SESSION['master_id'] = setAttendanceMaster($fid,$scode,$DB);
		$subject = getSubjectByCode($scode,$DB);
		$sem = $subject['semester'];
		$results = getStudentsBySemesterAndDepartment($sem,$dept,$DB);
	}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/style.css">
	<title>Take Attendance</title>
</head>
<body>
	<div class="wrapper">
		<!-- HEADER -->
		<div class="header">
			<div class="container">
				<div class="logo">
					<h1><a href="index.php">Attendance Manager</a></h1>
					<p>Take Attendance</p>
				</div>
				<div class="links">
					<ul class="top-links">
						<li><span>Welcome, <?php echo $_SESSION['username'];?></span></li>
						<li><a href="logout.php" class="button button-primary">Log Out</a></li>
					</ul>
				</div>
			</div>
		</div>

		<!-- CONTENT AREA -->
		<div class="content-wrapper">
			<div class="container">
				<div class="content">
					<div class="sidebar">
						<ul class="side-links">
							<li><a href="#" class="button button-full-width button-side-links">Edit Profile</a></li>
						</ul>
					</div>
					<div class="main-content">
						<div class="personal-details">
							<h2>Subject Information</h2>
							<div class="details">
								<ul>
									<li><span class="bold">Subject Name : </span><?php echo $subject['subject_name'] ; ?></li>
									<li><span class="bold">Subject Code: </span><?php echo $subject['subject_code'] ?></li>
									<li><span class="bold">Credits: </span><?php echo $subject['subject_credits']; ?></li>
								</ul>
							</div>
						</div>
						
						<div class="clear"></div>
						<div class="assigned-classes">
							<h2>Students</h2>
							<div class="table-large">
								<form action="" method="POST">
									<?php  foreach($results as $s) {  ?>
										<label class="student-cb" for="<?php echo $s['enrollment_number']; ?>">
										    <div>
										    	<p><?php echo $s['enrollment_number']; ?></p>
										        <input style="float: right;" 
										        		value="<?php echo $s['student_id']; ?>" 
										        		id="<?php echo $s['enrollment_number']; ?>" 
										        		name="cb[]" 
										        		type="checkbox">
										    </div>
										</label>
										<div class="clear"></div>
									<?php } ?>
									<input class="button button-warning" type="submit" value="Commit"/>
								</form>
							</div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		
		<!-- FOOTER -->
		<div class="footer">
			<span class="copyright">Copyright </span>
			<span class="year"> 2014</span>
		</div>
	</div>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script>
	$(":checkbox").on('click', function(){
     $(this).parent().toggleClass("present");
	});
	</script>
</body>
</html>
<?php

}

else
{
	header('HTTP/1.0 403 Forbidden');
	echo '403 Forbidden';
	exit();
}