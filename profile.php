<?php

require("includes/init.inc.php");

if(!isset($_SESSION['user_id']))
{
	header("Location: login.php");
	exit();
}

if($_SESSION['userlevel'] == 0)
{
	header("Location: admin/index.php");
	exit();
}

if($_SESSION['userlevel'] == 1)
{
	$uid = $_SESSION['user_id'];
	$result = getFacultyByUserID($uid,$DB);
	$fid = $result['faculty_id'];
	$fn = $result['firstname'];
	$ln = $result['lastname'];
	$dept = $result['dept_code'];
	$result = getSubjectsAssignedToFaculty($fid,$DB);
	$scount = count($result);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/style.css">
	<title>Faculty Information</title>
</head>
<body>
	<div class="wrapper">
		<!-- HEADER -->
		<div class="header">
			<div class="container">
				<div class="logo">
					<h1><a href="index.php">Attendance Manager</a></h1>
					<p>Profile</p>
				</div>
				<div class="links">
					<ul class="top-links">
						<li><span>Welcome, <?php echo $_SESSION['username'];?></span></li>
						<li><a href="logout.php" class="button button-primary">Log Out</a></li>
					</ul>
				</div>
			</div>
		</div>

		<!-- CONTENT AREA -->
		<div class="content-wrapper">
			<div class="container">
				<div class="content">
					<div class="sidebar">
						<ul class="side-links">
							<li><a href="#" class="button button-full-width button-side-links">Edit Profile</a></li>
						</ul>
					</div>
					<div class="main-content">
						<div class="personal-details">
							<h2>Faculty Information</h2>
							<div class="details">
								<ul>
									<li><span class="bold">Name : </span><?php echo $fn . ' ' . $ln; ?></li>
									<li><span class="bold">Department : </span><?php echo getDepartment($dept); ?></li>
									<li><span class="bold">Subjects Assigned : </span><?php echo $scount; ?></li>
								</ul>
							</div>
							<div class="image">
								<img src="http://placehold.it/140X100" alt="Placeholder">
							</div>
						</div>
						<div class="clear"></div>
						<div class="assigned-classes">
							<h2>Classes Assigned</h2>
							<div class="table-large">
								<table> 
									<thead>
										<tr>
											<th>Sr. No</th>
											<th>Semester</th>
											<th>Class</th>
											<th>Subjects</th>
											<th>Options</th>
										</tr>
									</thead>
									<tbody>
										<?php

											$count=1;
											foreach($result as $s)
											{
												if($count%2 == 0)
												{
													echo '<tr class="odd">';
												}
												else
												{
													echo '<tr class="even">';
												}

												echo '<td>' . $count . '</td>';
												echo '<td>' . $s['subject_code'] . '</td>';
												echo '<td>' . $s['semester'] . '</td>';
												echo '<td>' . $s['subject_name'] . '</td>';
												echo '<td><a href="take.php?subject=' . $s['subject_code'] . '">Take Attendance</a></td>';
												echo '</tr>';
												$count++;

											}

										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		
		<!-- FOOTER -->
		<div class="footer">
			<span class="copyright">Copyright </span>
			<span class="year"> 2014</span>
		</div>
	</div>
</body>
</html>
<?php

}

elseif($_SESSION['userlevel'] == 2)
{
	$uid = $_SESSION['user_id'];
	$query = $DB->query("SELECT * FROM students WHERE user_id='{$uid}'");
	$result = $query->fetch(PDO::FETCH_ASSOC);
	$sid = $result['student_id'];
	$fn = $result['firstname'];
	$ln = $result['lastname'];
	$sem = $result['semester'];
	$dept = $result['dept_code'];
	$result = getSubjectsBySemesterAndDepartment($sem,$dept,$DB);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/style.css">
	<title>Student Information</title>
</head>
<body>
	<div class="wrapper">
		<!-- HEADER -->
		<div class="header">
			<div class="container">
				<div class="logo">
					<h1><a href="#">Attendance Manager</a></h1>
					<p>Profile</p>
				</div>
				<div class="links">
					<ul class="top-links">
						<li><span>Welcome, <?php echo $_SESSION['username'];?></span></li>
						<li><a href="logout.php" class="button button-primary">Log Out</a></li>
					</ul>
				</div>
			</div>
		</div>

		<!-- CONTENT AREA -->
		<div class="content-wrapper">
			<div class="container">
				<div class="content">
					<div class="sidebar">
						<ul class="side-links">
							<li><a href="check.php" class="button button-full-width button-side-links">Check Attendance</a></li>
							<li><a href="#" class="button button-full-width button-side-links">Edit Profile</a></li>
						</ul>
					</div>
					<div class="main-content">
						<div class="personal-details">
							<h2>Student Information</h2>
							<div class="details">
								<ul>
									<li><span class="bold">Name: </span><?php echo $fn . ' ' . $ln; ?></li>
									<li><span class="bold">Department: </span><?php echo getDepartment($dept); ?></li>
									<li><span class="bold">Semester: </span><?php echo $sem; ?></li>
								</ul>
							</div>
							<div class="image">
								<img src="http://placehold.it/140X100" alt="Placeholder">
							</div>
						</div>
						<div class="clear"></div>
						<div class="assigned-classes">
							<h2>Classes Taken</h2>
							<div class="table-large">
								<table> 
									<thead>
										<tr>
											<th>Sr. No</th>
											<th>Semester</th>
											<th>Class</th>
											<th>Subjects</th>
											<th>Options</th>
										</tr>
									</thead>
									<tbody>
										<?php

											$count=1;
											foreach($result as $s)
											{
												if($count%2 == 0)
												{
													echo '<tr class="odd">';
												}
												else
												{
													echo '<tr class="even">';
												}

												echo '<td>' . $count . '</td>';
												echo '<td>' . $s['subject_code'] . '</td>';
												echo '<td>' . $s['semester'] . '</td>';
												echo '<td>' . $s['subject_name'] . '</td>';
												echo '<td><a href="check.php?subject=' . $s['subject_code'] . '">Check Attendance</a></td>';
												echo '</tr>';
												$count++;

											}

										?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
		
		<!-- FOOTER -->
		<div class="footer">
			<span class="copyright">Copyright </span>
			<span class="year"> 2014</span>
		</div>
	</div>
</body>
</html>
<?php

}