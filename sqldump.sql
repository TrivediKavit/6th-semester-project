-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 15, 2014 at 03:27 PM
-- Server version: 5.5.34
-- PHP Version: 5.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `attendance-manager`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendance_detail`
--

CREATE TABLE IF NOT EXISTS `attendance_detail` (
  `attendance_detail_id` bigint(11) NOT NULL AUTO_INCREMENT,
  `attendance_master_id` bigint(11) NOT NULL,
  `student_id` bigint(11) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`attendance_detail_id`),
  KEY `attendance_master_id` (`attendance_master_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;


-- --------------------------------------------------------

--
-- Table structure for table `attendance_master`
--

CREATE TABLE IF NOT EXISTS `attendance_master` (
  `attendance_master_id` bigint(11) NOT NULL AUTO_INCREMENT,
  `map_id` bigint(11) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`attendance_master_id`),
  KEY `map_id` (`map_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;


-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE IF NOT EXISTS `faculty` (
  `faculty_id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `dept_code` int(3) NOT NULL,
  PRIMARY KEY (`faculty_id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;


-- --------------------------------------------------------

--
-- Table structure for table `map_faculty_subject_semester`
--

CREATE TABLE IF NOT EXISTS `map_faculty_subject_semester` (
  `map_id` bigint(11) NOT NULL AUTO_INCREMENT,
  `faculty_id` bigint(11) NOT NULL,
  `subject_id` bigint(11) NOT NULL,
  PRIMARY KEY (`map_id`),
  KEY `faculty_id` (`faculty_id`),
  KEY `subject_id` (`subject_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE IF NOT EXISTS `students` (
  `student_id` bigint(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(11) NOT NULL,
  `enrollment_number` bigint(12) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `semester` int(1) NOT NULL,
  `dept_code` int(3) NOT NULL,
  PRIMARY KEY (`student_id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;


-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE IF NOT EXISTS `subjects` (
  `subject_id` bigint(11) NOT NULL AUTO_INCREMENT,
  `subject_code` int(6) NOT NULL,
  `subject_name` varchar(32) NOT NULL,
  `subject_credits` int(1) NOT NULL,
  `semester` int(1) NOT NULL,
  `dept_code` int(3) NOT NULL,
  PRIMARY KEY (`subject_id`),
  UNIQUE KEY `subject_code` (`subject_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;


-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` bigint(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `password` varchar(60) NOT NULL,
  `email` varchar(64) NOT NULL,
  `userlevel` int(11) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `email`,`userlevel`) VALUES
(1, 'admin', 'admin', 'admin@admin.com', 0);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `attendance_detail`
--
ALTER TABLE `attendance_detail`
  ADD CONSTRAINT `attendance_detail_ibfk_1` FOREIGN KEY (`attendance_master_id`) REFERENCES `attendance_master` (`attendance_master_id`);

--
-- Constraints for table `attendance_master`
--
ALTER TABLE `attendance_master`
  ADD CONSTRAINT `attendance_master_ibfk_1` FOREIGN KEY (`map_id`) REFERENCES `map_faculty_subject_semester` (`map_id`);

--
-- Constraints for table `faculty`
--
ALTER TABLE `faculty`
  ADD CONSTRAINT `faculty_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `map_faculty_subject_semester`
--
ALTER TABLE `map_faculty_subject_semester`
  ADD CONSTRAINT `map_faculty_subject_semester_ibfk_1` FOREIGN KEY (`faculty_id`) REFERENCES `faculty` (`faculty_id`),
  ADD CONSTRAINT `map_faculty_subject_semester_ibfk_2` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`);

--
-- Constraints for table `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `students_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
